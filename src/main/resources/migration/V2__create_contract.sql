CREATE TABLE `contracts`
(
    `id`        BIGINT(15)   NOT NULL,
    `name`      VARCHAR(128) NOT NULL,
    `client_id` BIGINT(15)   NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
