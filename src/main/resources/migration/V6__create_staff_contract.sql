CREATE TABLE `contact_office`
(
    `id`          BIGINT(15) NOT NULL,
    `contract_id` BIGINT(15),
    `office_id`   BIGINT(15) NOT NULL,
    `admin_id`    BIGINT(15) NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `contract_offices_fk` FOREIGN KEY (contract_id) REFERENCES contracts (id),
    CONSTRAINT `office_fk` FOREIGN KEY (office_id) REFERENCES offices (id),
    CONSTRAINT `office_staff_fk` FOREIGN KEY (admin_id) REFERENCES staff (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
