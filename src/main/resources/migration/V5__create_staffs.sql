CREATE TABLE `staff`
(
    `id`         BIGINT(15)  NOT NULL,
    `first_name` VARCHAR(25) NOT NULL,
    `last_name`  VARCHAR(25) NOT NULL,
    `office_id`  BIGINT(15)  NOT NULL,
    PRIMARY KEY (id),
    CONSTRAINT `staff_offices_fk` FOREIGN KEY (office_id) REFERENCES offices (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
