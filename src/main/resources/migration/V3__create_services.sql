CREATE TABLE `services`
(
    `id`   BIGINT(15)  NOT NULL,
    `type` VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
