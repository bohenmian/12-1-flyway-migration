CREATE TABLE `offices`
(
    `id`      BIGINT(15)  NOT NULL,
    `country` VARCHAR(25) NOT NULL,
    `city`    VARCHAR(25) NOT NULL,
    PRIMARY KEY (id)
) ENGINE = InnoDB
  DEFAULT CHARSET = latin1;
