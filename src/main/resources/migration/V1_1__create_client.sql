CREATE TABLE `clients`
(
    `id`           BIGINT(15)   NOT NULL,
    `full_name`    VARCHAR(128) NOT NULL,
    `abbreviation` VARCHAR(6)   NOT NULL,
    PRIMARY KEY (`id`)
) ENGINE = InnoDB DEFAULT CHARSET = latin1;
